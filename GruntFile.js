module.exports = function (grunt) {

    grunt.initConfig({
        clean: ["deploy"],
        copy: {
            main: {
                expand: true,
                cwd: 'src/',
                src: '**',
                dest: 'deploy/',
            },
            packagejson: {
                src: 'package.json',
                dest: 'deploy/package.json',
            }
        },
        uglify: {
            options: {
                mangle: true,
                compress: false,
                sourceMap: true
            },
            js: {
                files: {
                    'deploy/sparkling-indexed.min.js': ['deploy/sparkling-indexed.js']
                }
            }
        },
        run: {
            options: {
                cwd: "deploy"
            },
            npm: {
                exec: "npm pack"
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-run');
    grunt.registerTask('default', ['clean', 'copy', 'uglify', 'run:npm']);
};