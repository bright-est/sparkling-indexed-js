if (typeof sparkling === "undefined") {
    sparkling = {};
}

(function(ns) {

	ns.Indexed = function() {
		var _this = this;

		ns.Observable.mixin(_this);

		/**
		 * Should be used when indexed columns are unique
		 */
		this.regIndexOptimistic = function(ref, addEventSubscriber, removeEventSubscriber) {
			var indexCols = [].splice.call(arguments, 3);
			regIndex.call(
				this, 
				ref, 
				addEventSubscriber, 
				removeEventSubscriber, 
				indexCols, 
				addObjectToIndexOptimistic, 
				removeObjectFromIndexOptimistic
			);
		};

		/*
		 * Register new index.
		 * Subscribes to events which will add or remove item from index
		 * @param {object} ref -
		 * @param {string} addEventName -
		 * @param {string} removeEventName -
		 * @param {...string} cols - Index columns.
		 */
		this.regIndex = function(ref, addEventSubscriber, removeEventSubscriber) {
			var indexCols = [].splice.call(arguments, 3);
			regIndex.call(
				this, 
				ref, 
				addEventSubscriber, 
				removeEventSubscriber, 
				indexCols, 
				addObjectToIndex, 
				removeObjectFromIndex
			);
		};

		/**
		 *
		 */
		function regIndex(ref, addSubscriber, removeSubscriber, indexCols, addFunc, removeFunc) {

			if (typeof addSubscriber === "string") {
				var addEventName = addSubscriber;
				addSubscriber = function (handler) {
					this.subscribe(addEventName, handler);
				}
			}
			else if (typeof addSubscriber !== "function") {
				throw new Error("Invalida argument. 'addSubsriber' must be 'string' or 'funciton'.");
			}

			if (typeof removeSubscriber === "string") {
				var removeEventName = removeSubscriber;
				removeSubscriber = function (handler) {
					this.subscribe(removeEventName, handler);
				}
			}
			else if (typeof removeSubscriber !== "function") {
				throw new Error("Invalida argument. 'addSubsriber' must be 'string' or 'funciton'.");
			}

			addSubscriber(function(object) {
				var temp = ref;
				for (var i = 0; i < indexCols.length - 1; i++) {

					var propName = indexCols[i];
					var propValue = getValue(object, propName);
					if (!temp[propValue]) {
						temp[propValue] = {};
					}
					temp = temp[propValue];
				}

				var x = indexCols[indexCols.length - 1];
				var indexedValue = getValue(object, x);

				addFunc(temp, indexedValue, object);
			});

			function remove(ix, object, cols) {
				var propName = cols[0];
				var propValue = getValue(object, propName);

				if (cols.length === 1) {
					removeFunc(ix, propValue, object);
					return;
				}

				remove(ix[propValue], object, cols.splice(1));

				if (isEmpty(ix[propValue])) {
					delete ix[propValue];
				}
			}

			removeSubscriber(function(o) {
				var cols = indexCols.slice(0);
				remove(ref, o, cols);
			});
		}

		/**
		 *
		 */
		function addObjectToIndex(temp, xval, o) {
			if (!temp[xval]) {
				temp[xval] = [];
			}
			temp[xval].push(o);
		}

		/**
		 *
		 */
		function removeObjectFromIndex(ix, propValue, o) {
			ix[propValue] = grep(ix[propValue], function(i) {
				return i !== o;
			});
			if (ix[propValue].length === 0) {
				delete ix[propValue];
			}
		}

		/**
		 *
		 */
		function addObjectToIndexOptimistic(temp, xval, o) {
			temp[xval] = o;
		}

		/**
		 *
		 */
		function removeObjectFromIndexOptimistic(ix, propValue) {
			delete ix[propValue];
		}

		/**
		 * Get nested value from object
		 * @param {object} source - source object
		 * @param {string} propName - property expression
		 */
		function getValue(source, propName) {
			var segments = propName.split(".");
			for (var i = 0; i < segments.length; i++) {

				var p = segments[i];
				if (p in source) {
					source = source[p];
				}
				else {
					return undefined;
				}
			}
			return source;
		}

		/*
		 * Check if is empty object
		 * @param {object} obj
		 */
		function isEmpty(obj) {

			// null and undefined are "empty"
			// or it is an empty array
			if (obj === null || obj.length === 0)
				return true;

			// non-empty array
			if (obj.length > 0)
				return false;

			// Otherwise, does it have any properties of its own?
			// Note that this doesn't handle
			// toString and valueOf enumeration bugs in IE < 9
			for (var key in obj) {
				if (obj.hasOwnProperty(key))
					return false;
			}

			return true;
		}

		function grep(items, isValid) {
			var filtered = [];
			for (var i = 0, len = items.length; i < len; i++) {
				var item = items[i];
				if (isValid(item)) {
					filtered.push(item);
				}
			}
			return filtered;
		}
	};

})(sparkling);