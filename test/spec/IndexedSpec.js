describe("A indexed object", function() {
	it("should have function regIndex", function() {

		var o = {};
		sparkling.Indexed.call(o);
    	expect(typeof o.regIndex).toBe("function");
	});
});