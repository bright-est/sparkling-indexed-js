# Sparkling.Indexed

This library depends on sparkling.Observable.

## regIndexOptimistic(ref, addEventSubscriber, removeEventSubscriber, col[, col2[, ...]])

This function is used to handle indexes by unique values - ids for instance.

### Parameters

ref (object): reference object where index will be stored
addEventSubscriber (string | function): 
removeEventSubscriber (string | function): 
col, col2, ... (string): index columns

## regIndex



## Performance

|  | Pessimistic indexing - Create Lookup | Optimistic indexing - Create Dictionary. Last indexed column has always 1 item. Last one wins |
|---|--------------------------------------|-----------------------------------------------------------------------------------------------|
| 1k items. Not likely one needs to initialize bigger collection. Basically does not take time at all. |
| 10k items. Still - blink of an eye |
| 100k items. Not very likely, but library can manage quite OK |
| 1 000 000 items. Just for benchmark |                                  |                                                                               |